-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 02, 2017 at 09:40 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `massmanagement`
--

-- --------------------------------------------------------

--
-- Table structure for table `mealentry`
--

CREATE TABLE `mealentry` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `mealDate` date NOT NULL,
  `breakfastMeal` int(11) NOT NULL,
  `breakfastGuest` int(11) NOT NULL,
  `lunchMeal` int(11) NOT NULL,
  `lunchGuest` int(11) NOT NULL,
  `dinnerMeal` int(11) NOT NULL,
  `dinnerGuest` int(11) NOT NULL,
  `totalMeal` int(11) NOT NULL,
  `totalGuestMeal` int(11) NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mealentry`
--

INSERT INTO `mealentry` (`id`, `userId`, `mealDate`, `breakfastMeal`, `breakfastGuest`, `lunchMeal`, `lunchGuest`, `dinnerMeal`, `dinnerGuest`, `totalMeal`, `totalGuestMeal`, `updated`) VALUES
(50, 8, '2016-12-22', 0, 0, 1, 323, 0, 0, 1, 323, '0000-00-00 00:00:00'),
(51, 7, '2016-11-01', 0, 0, 0, 323, 0, 0, 0, 323, '0000-00-00 00:00:00'),
(52, 8, '2016-12-01', 1, 0, 1, 25, 0, 0, 2, 25, '0000-00-00 00:00:00'),
(53, 8, '2016-11-15', 0, 0, 1, 234234, 0, 0, 1, 234234, '0000-00-00 00:00:00'),
(54, 6, '2016-12-10', 1, 0, 1, 0, 1, 0, 3, 0, '0000-00-00 00:00:00'),
(55, 5, '2016-12-05', 1, 0, 1, 0, 0, 3, 2, 3, '0000-00-00 00:00:00'),
(56, 6, '2016-12-02', 1, 0, 1, 0, 1, 3, 3, 3, '0000-00-00 00:00:00'),
(57, 7, '2016-10-01', 1, 0, 0, 0, 0, 3, 1, 3, '0000-00-00 00:00:00'),
(58, 8, '2017-01-01', 1, 1, 1, 1, 1, 1, 3, 3, '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mealentry`
--
ALTER TABLE `mealentry`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mealentry`
--
ALTER TABLE `mealentry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
