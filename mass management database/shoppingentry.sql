-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 02, 2017 at 09:40 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `massmanagement`
--

-- --------------------------------------------------------

--
-- Table structure for table `shoppingentry`
--

CREATE TABLE `shoppingentry` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `shoppingDate` varchar(255) NOT NULL,
  `shoppingDiscription` varchar(255) NOT NULL,
  `totalTk` int(11) NOT NULL,
  `creaded` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shoppingentry`
--

INSERT INTO `shoppingentry` (`id`, `userId`, `shoppingDate`, `shoppingDiscription`, `totalTk`, `creaded`, `updated`) VALUES
(1, 9, '25-Nov-2016', ' ric', 33, '0000-00-00 00:00:00', '2016-10-29 11:57:35'),
(2, 9, '04-Nov-2016', ' rice   ', 1100, '2016-10-24 02:21:18', '2016-10-30 01:00:08'),
(3, 9, '24-Oct-2016', ' rice ', 210, '2016-10-24 02:24:11', '2016-10-24 02:24:11'),
(4, 8, '24-Oct-2016', ' alo. potol, piaj ', 120, '2016-10-24 02:25:50', '2016-10-29 10:05:03'),
(5, 8, '26-Oct-2016', ' gsdfgsfd  ', 5500, '2016-10-24 02:27:50', '2016-11-10 02:51:13'),
(6, 8, '06-Oct-2016', ' fhgrdhgdf', 2, '2016-10-24 02:30:47', '2016-10-24 02:30:47'),
(7, 8, '24-Oct-2016', 'dhdtgrh', 1, '2016-10-24 02:31:47', '2016-10-24 02:31:47'),
(8, 8, '24-Oct-2016', ' dhsdfghsdfg', 4, '2016-10-24 02:36:20', '2016-10-24 02:36:20'),
(9, 7, '24-Nov-2016', ' rice  ', 210, '2016-10-27 01:08:34', '2016-10-29 11:49:58'),
(10, 9, '29-Oct-2016', ' xdfhjn', 410, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 9, '27-Oct-2016', ' gbjhbh', 320, '2016-10-30 02:17:07', '0000-00-00 00:00:00'),
(12, 6, '02-Nov-2016', ' bsdgbsrg', 214, '2016-11-02 01:23:18', '0000-00-00 00:00:00'),
(13, 3, '03-Nov-2016', ' rice', 250, '2016-11-03 03:32:45', '0000-00-00 00:00:00'),
(14, 5, '03-Nov-2016', ' telll', 456, '2016-11-03 03:34:15', '0000-00-00 00:00:00'),
(15, 5, '02-Nov-2016', ' telll  ', 456, '2016-11-03 03:35:00', '0000-00-00 00:00:00'),
(16, 9, '09-Nov-2016', ' fthjrt', 100, '2016-11-10 06:43:09', '0000-00-00 00:00:00'),
(17, 5, '09-Nov-2016', ' gsfg', 1000, '2016-11-10 07:31:33', '0000-00-00 00:00:00'),
(18, 6, '10-Nov-2016', '  thrdthstr', 360, '2016-11-10 02:10:20', '0000-00-00 00:00:00'),
(19, 4, '31-Oct-2016', '  b ncn', 321, '2016-11-10 03:23:10', '0000-00-00 00:00:00'),
(20, 9, '07-Dec-2016', ' xfgndy', 1000, '2016-12-25 08:27:10', '0000-00-00 00:00:00'),
(21, 8, '01-Jan-2017', ' sdfdsa', 100, '2017-01-02 02:35:08', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `shoppingentry`
--
ALTER TABLE `shoppingentry`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `shoppingentry`
--
ALTER TABLE `shoppingentry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
