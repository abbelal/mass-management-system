<?php

class resistrationLogin {

    public $id = '';
    public $uniqueId = '';
    public $fullName = '';
    public $fatherName = '';
    public $eduTitle = '';
    public $email = '';
    public $password = '';
    public $confirmPassword = '';
    public $phone = '';
    public $nationalId = '';
    public $datepicker = '';
    public $address = '';
    public $city = '';
    public $zip = '';
    public $fullAddress = '';
    public $image = '';
    
    public $dbConnect = '';
    public $dbUser = 'root';
    public $dbpassword = '';
    public $error = '';
    
    public $rememberMe = '';
    public $dbAllUser = '';
    public $blockedAllData = '';

    public function __construct() {
        if (empty(session_id())) {
            session_start();
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->dbConnect = new PDO("mysql:host=localhost; dbname=massmanagement", $this->dbUser, $this->dbpassword) or die("Unable to Connect Database");
    }

    public function prepare($formAllData='') {
        if (array_key_exists('id', $formAllData) && !empty($formAllData['id'])) {
            $this->id = $formAllData['id'];
        }

        if (array_key_exists('uniqueId', $formAllData) && !empty($formAllData['uniqueId'])) {
            $this->uniqueId = $formAllData['uniqueId'];
        }

        if (array_key_exists('fullName', $formAllData)) {
            $this->fullName = $formAllData['fullName'];
        }

        if (array_key_exists('fatherName', $formAllData)) {
            $this->fatherName = $formAllData['fatherName'];
        }

        if (array_key_exists('eduTitle', $formAllData)) {
            $this->eduTitle = $formAllData['eduTitle'];
        }

        if (array_key_exists('email', $formAllData)) {
            $this->email = $formAllData['email'];
        }

        if (array_key_exists('password', $formAllData)) {
            $this->password = $formAllData['password'];
        }

        if (array_key_exists('confirmPassword', $formAllData)) {
            $this->confirmPassword = $formAllData['confirmPassword'];
        }

        if (array_key_exists('nationalId', $formAllData)) {
            $this->nationalId = $formAllData['nationalId'];
        }

        if (array_key_exists('phone', $formAllData)) {
            $this->phone = $formAllData['phone'];
        }

        if (array_key_exists('date', $formAllData)) {
            $this->datepicker = $formAllData['date'];
        }

        if (array_key_exists('address', $formAllData)) {
            $this->address = $formAllData['address'];
        }

        if (array_key_exists('city', $formAllData)) {
            $this->city = $formAllData['city'];
        }

        if (array_key_exists('zip', $formAllData)) {
            $this->zip = $formAllData['zip'];
        }
        $fullAddres = array("$this->address", "$this->city", "$this->zip");
        $this->fullAddress = serialize($fullAddres);

        if (array_key_exists('image', $formAllData)) {
            $this->image = $formAllData['image'];
        }

        if (array_key_exists('rememberMe', $formAllData)) {
            $this->rememberMe = $formAllData['rememberMe'];
        }

//        echo '<pre>';
//        print_r($formAllData);
        $_SESSION['formData'] = $formAllData;
    }

    //end preparen method
    //
    //
    //
    //
    //
    public function msgEcho($msg='') {
        if (isset($_SESSION["$msg"]) && !empty($_SESSION["$msg"])) {
            echo $_SESSION["$msg"];
            unset($_SESSION["$msg"]);
        }
    }

    //end sesstion echo Methode
    //
    //
    //
    //
    //
    //
    public function formValidation() {

        //full name validation
        if (!empty($this->fullName)) {
            if (strlen($this->fullName) >= 5 && strlen($this->fullName) <= 30) {
                //echo "requirment fullfill";
            } else {
                $_SESSION['nameErr'] = "Name must be 5 to 20 character";
                $this->error = TRUE;
            }
        } else {
            $_SESSION['nameErr'] = "Name Required";
            $this->error = TRUE;
        }

        //email validation
        if (!empty($this->email)) {
            if (strlen($this->email) <= 30) {
                if (!filter_var($this->email, FILTER_VALIDATE_EMAIL) === FALSE) {
                    $email = "'$this->email'";
                    $seletctQuery = "SELECT * FROM `memberresistration` WHERE `email`= " . $email;
                    $stmt = $this->dbConnect->prepare($seletctQuery);
                    $stmt->execute();
                    $validEmilRow = $stmt->fetch(PDO::FETCH_ASSOC);

                    if (!empty($validEmilRow)) {
                        if ($this->uniqueId != $validEmilRow['uniqueId']) {
                            $_SESSION['emailErr'] = "Email already Exist try to another one";
                            $this->error = TRUE;
                        }
                    }
                } else {
                    $_SESSION['emailErr'] = "Email is not valid";
                    $this->error = TRUE;
                }
            } else {
                $_SESSION['emailErr'] = "Email must be within 20 character";
                $this->error = TRUE;
            }
        } else {
            $_SESSION['emailErr'] = "Email Required";
            $this->error = TRUE;
        }

        //password validation
        if (!empty($this->password)) {
            if (strlen($this->password) >= 6 && strlen($this->password) <= 12) {
                if (!empty($this->confirmPassword)) {
                    if ($this->password != $this->confirmPassword) {
                        $_SESSION['repPasswordErr'] = "Password not Matched";
                        $this->error = TRUE;
                    }
                } else {
                    $_SESSION['repPasswordErr'] = "Confirm Password";
                    $this->error = TRUE;
                }
            } else {
                $_SESSION['passwordErr'] = "Password must be <span style='font-weight:bold;'>6 to 12 </span> character";
                $this->error = TRUE;
            }
        } else {
            $_SESSION['passwordErr'] = "Password Required";
            $this->error = TRUE;
        }

        //phone validation
        if (!empty($this->phone)) {
            if (strlen($this->phone) != 11) {
                $_SESSION['phoneErr'] = "Phone number must be 11 Number";
                $this->error = TRUE;
            }
        } else {
            $_SESSION['phoneErr'] = "Phone Number Required";
            $this->error = TRUE;
        }

        //national id validation
        if (empty($this->nationalId)) {
            $_SESSION['nidErr'] = "National ID Requried";
            $this->error = TRUE;
        }

        //address validation
        if (empty($this->address)) {
            $_SESSION['addressErr'] = "Address Requried";
            $this->error = TRUE;
        }

        //city validation
        if (empty($this->city)) {
            $_SESSION['cityErr'] = "City Name Requried";
            $this->error = TRUE;
        }

        //zip validation
        if (empty($this->zip)) {
            $_SESSION['zipErr'] = "Zip Code Requried";
            $this->error = TRUE;
        }
    }

    //end formValidation Methode
    //
    //
    //
    //
    //
    //
       
    public function editEmailValidation() {
        //email validation
        if (!empty($this->email)) {
            if (strlen($this->email) <= 30) {
                if (!filter_var($this->email, FILTER_VALIDATE_EMAIL) === FALSE) {
                    $this->error = FALSE;
                } else {
                    $_SESSION['emailErr'] = "Email is not valid";
                    $this->error = TRUE;
                }
            } else {
                $_SESSION['emailErr'] = "Email must be within 20 character";
                $this->error = TRUE;
            }
        } else {
            $_SESSION['emailErr'] = "Email Required";
            $this->error = TRUE;
        }
    }

    //end formValidation Methode for user update 
    //
    //
    //
    //
    //
    //
    public function imageValidation() {

        $this->image = uniqid() . ' ' . $_FILES['image']['name'];
        $imageType = $_FILES['image']['type'];
        $imageTempName = $_FILES['image']['tmp_name'];
        $imageSize = $_FILES['image']['size'];

        $myImageFormate = strtolower(end(explode('.', $this->image)));
        $requiredFormate = array('jpeg', 'jpg', 'png', 'gif');

        if (!in_array($myImageFormate, $requiredFormate)) {
            $_SESSION['imageErr'] = "jpeg , jpg , png , gif file formate Required";
            $this->error = TRUE;
        } else {
            if ($imageSize > 2097152) {
                $_SESSION['imageErr'] = "File Size must be Under 2 MB";
                $this->error = TRUE;
            } else {
                move_uploaded_file($imageTempName, "images/massMemberImage/" . $this->image);
            }
        }
    }

    //end mamber image validation Methode
    //
    //
    //
    //
    //
    //
   
    
    public function addMember() {
        if ($this->error == FALSE) {
            try {
                $uniqueid = uniqid();
                $insertQuery = "INSERT INTO `memberresistration` (`id`, `uniqueId`, `name`, `fatherName`, `education`, `email`, `password`, `phone`, `nationalid`, `address`, `image`, `date`, `created`) 
                    VALUES ( :id, :uniqueid, :name, :fathername, :education, :email, :password, :phone, :nationalid, :address, :image, :adddate, :created)";
                $stmt = $this->dbConnect->prepare($insertQuery);
                $stmt->execute(array(
                    ':id' => NULL,
                    ':uniqueid' => $uniqueid,
                    ':name' => $this->fullName,
                    ':fathername' => $this->fatherName,
                    ':education' => $this->eduTitle,
                    ':email' => $this->email,
                    ':password' => $this->password,
                    ':phone' => $this->phone,
                    ':nationalid' => $this->nationalId,
                    ':address' => $this->fullAddress,
                    ':image' => $this->image,
                    ':adddate' => $this->datepicker,
                    ':created' => date("Y-m-d h:i:s"),
                ));
                $_SESSION['addSuccessMSg'] = "A New Mass Member Successfully Added";
                header("location:memberAdd.php");
            } catch (PDOException $e) {
                echo 'Error: ' . $e->getMessage();
            }
        } else {
            header('location:memberAdd.php');
        }
    }

    //end add member Methode
    //
    //
    //
    //
    //
    //
    public function showAllMassMember() {
        try {
            $selectQuery = "SELECT * FROM `memberresistration` WHERE `isDelete`= 0 ORDER BY `id` DESC";
            $stmt = $this->dbConnect->prepare($selectQuery);
            $stmt->execute();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $this->dbAllUser[] = $row;
            }
            return $this->dbAllUser;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    //
    //
    //
    //
    //end member show method

    public function showSingleMember() {
        $uniqueid = "'$this->uniqueId'";
        $selectQuery = "SELECT * FROM `memberresistration` WHERE `uniqueId`=" . $uniqueid;
        $stmt = $this->dbConnect->prepare($selectQuery);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    //
    //
    //
    //
    //
    //end member show method

    public function byUserInfoUpdate() {
        if ($this->error == FALSE) {
            try {
                $uniqueId = "'$this->uniqueId'";
                //$updateQuery = "UPDATE `memberresistration` SET `email` = $this->email, `password` = $this->password, `image` = $this->image WHERE `memberresistration`.`uniqueId` = " . $uniqueId;
                $updateQuery = "UPDATE `memberresistration` SET `email` = '$this->email', `password` = '$this->password', `image` = '$this->image', `updated` = :updated WHERE `memberresistration`.`uniqueId` = " . $uniqueId;
//              echo $updateQuery;
//                die();
                $stmt = $this->dbConnect->prepare($updateQuery);
                 $stmt->execute(array(
//                    ':email' => $this->email,
//                    ':password' => $this->password,
//                    ':image' => $this->image,
                    ':updated' => date("Y-m-d h:i:s")
                ));
                $_SESSION['updateSuccessMSg'] = "Your Information Successfully Updated";
                header("location:edit.php?uniqueId=$this->uniqueId");
            } catch (PDOException $e) {
                echo 'Error: ' . $e->getMessage();
            }
        } else {
            header("location:edit.php?uniqueId=$this->uniqueId");
        }
    }

    //
    //
    //
    //
    //
    //end member update method
    
    
    public function memberBlock(){
        try {
            $uniqueId = "'$this->uniqueId'";
            $updateQuery = "UPDATE `memberresistration` SET `isDelete`=:isDelete, `deleted`=:deleteDate WHERE `memberresistration`.`uniqueId`= " . $uniqueId;
            $stmt = $this->dbConnect->prepare($updateQuery);
            $stmt->execute(array(
                ':isDelete' => 1,
                ':deleteDate' => date("Y-m-d h:i:s"),
            ));
            $_SESSION['trashSuccess'] = "A Member Successfully Blocked";
            header('location:allMassMembers.php');
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    //
    //
    //
    //
    //
    //end member block method
    
    public function memberBlockList(){
       try {
            $selectQuery = "SELECT * FROM `memberresistration` WHERE `isDelete`= 1 ORDER BY `id` DESC";
            $stmt = $this->dbConnect->prepare($selectQuery);
            $stmt->execute();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $this->blockedAllData[] = $row;
            }
            return $this->blockedAllData;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        } 
    }

    //
    //
    //
    //
    //
    //end member block list method
    
    public function memberUnblock(){
        try {
            $uniqueId = "'$this->uniqueId'";
            $updateQuery = "UPDATE `memberresistration` SET `isDelete`=:isDelete WHERE `memberresistration`.`uniqueId`= " . $uniqueId;
            $stmt = $this->dbConnect->prepare($updateQuery);
            $stmt->execute(array(
                ':isDelete' => 0,
            ));
            $_SESSION['unblockSuccess'] = "A Member Successfully Unblocked";
            header('location:trashList.php');
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    //
    //
    //
    //
    //
    //end member Unblock method
    

    public function login() {
        $loginEmail = "'$this->email'";
        $loginPassword = "'$this->password'";

        $selectQuery = "SELECT * FROM `memberresistration` WHERE `email`= $loginEmail AND `password`=$loginPassword";
        $stmt = $this->dbConnect->prepare($selectQuery);
        $stmt->execute();
        $userRow = $stmt->fetch(PDO::FETCH_ASSOC);

        if (!empty($userRow['email']) && !empty($userRow['password'])) {
            if ($userRow['isActive'] == 1) {

                if (!empty($this->rememberMe)) {
                    setcookie("email", $this->email, time() + (10 * 365 * 24 * 60 * 60));
                    setcookie("password", $this->password, time() + (10 * 365 * 24 * 60 * 60));
                } else {
                    if (isset($_COOKIE["email"])) {
                        setcookie("email", "");
                    }
                    if (isset($_COOKIE["password"])) {
                        setcookie("password", "");
                    }
                }

                $_SESSION['loginedUser'] = $userRow;
                header('location:allMassMembers.php');
            } else {
                $_SESSION['loginErr'] = "Your account not verified yet";
                header('location:login.php');
            }
        } else {
            $_SESSION['loginErr'] = "Invelid User Name of Password";
            header('location:login.php');
        }
    }

    //end login Methode
    //
    //
    //
    //
    //
    //
    public function logout() {
        try {
            unset($_SESSION['loginedUser']);
            $_SESSION['logoutMsg'] = "Successfully logout";
            header("location:login.php");
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    //end logout method
//
//
//
//
}

//end class
?>
