<?php 
    include_once '../src/resistrationLogin/resistrationLogin.php';
    $objResistration=new resistrationLogin();

?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="csstransforms no-csstransforms3d csstransitions" lang="en"> <!--<![endif]-->
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mass Management</title>
        <link rel="shortcut icon" type="image/png" href="images/favicon.png"/>

        <!-- font-awesome -->
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

        <!-- google font -->
        <link href="https://fonts.googleapis.com/css?family=Bungee" rel="stylesheet"> 

        <!-- bootstrap CDN-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="css/index.page.style.css" />
    <body>
        <div class="login-form-wrapper">
            <form action="loginProcess.php" method="POST">
                <p class="text-danger text-center" style="font-size: 13px;"><?php $objResistration->msgEcho('loginErr'); $objResistration->msgEcho('pageErr'); $objResistration->msgEcho('logoutMsg') ?></p>
                <div class="form-group">
                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Email Address" value="<?php if (isset($_COOKIE["email"])) { echo $_COOKIE["email"]; } ?>">
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password" value="<?php if (isset($_COOKIE["password"])) { echo $_COOKIE["password"]; }?>">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-default">Login</button>
                    <div class="checkbox"><label><input type="checkbox" name="rememberMe" <?php if (isset($_COOKIE["email"])) { ?> checked <?php } ?><label> Remember me</label></label></div>
                </div>                
            </form>
        </div>
        <img class="bird" src="images/bird.png" class="img img-responsive" alt="home-img" />
        <img class="logo" src="images/loog.png" class="img img-responsive" alt="home-img" />
    </body>
</html>