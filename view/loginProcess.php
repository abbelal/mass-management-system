<?php

include_once '../src/resistrationLogin/resistrationLogin.php';
$objResistration = new resistrationLogin();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $objResistration->prepare($_POST);
    $objResistration->login();
} else {
    $_SESSION['pageError'] = "<h1>404 page not found <a href='login.php'>Click to Login</a></h1>";
    header('location:error.php');
}
?>
