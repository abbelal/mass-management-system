<?php
include_once '../src/resistrationLogin/resistrationLogin.php';
$objResistration = new resistrationLogin();
$allMemberData = $objResistration->showAllMassMember();

//print_r($allMemberData);
//print_r($_SESSION['loginedUser']);
if (isset($_SESSION['loginedUser']) && !empty($_SESSION['loginedUser'])) {
    ?>

    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>Add Meal</title>
            <link rel="shortcut icon" type="image/png" href="images/favicon.png"/>

            <!-- google font CDN -->
            <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">

            <!--bootstrap CDN-->
            <!-- Latest compiled and minified CSS -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

            <!-- Optional theme -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

            <!-- Global stylesheets -->
            <link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
            <link href="assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
            <link href="assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
            <link href="assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
            <!-- /global stylesheets -->


            <!-- my all custom css file-->
            <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
            <link href="css/memberAddFromStyle.css" rel="stylesheet" type="text/css">
            <link href="css/mealEntryFormDesign.css" rel="stylesheet" type="text/css">

        </head>

        <body>
            <!-- Main navbar -->
            <div class="navbar navbar-inverse custom-style">
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html"><img src="assets/images/app-loog.png" alt="app-logo"></a>

                    <ul class="nav navbar-nav visible-xs-block">
                        <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                        <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
                    </ul>

                </div>

                <div class="navbar-collapse collapse" id="navbar-mobile">
                    <ul class="nav navbar-nav">
                        <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown dropdown-user">
                            <a class="dropdown-toggle" data-toggle="dropdown">
                                <img height="46px" width="46px" src="images/massMemberImage/<?php echo $_SESSION['loginedUser']['image'] ?>" alt="user image">
                                <span><?php echo ucfirst($_SESSION['loginedUser']['name']); ?><br/></span>
                                <i class="caret"></i><br/>
                                <span class="admin">
                                    <?php
                                    if ($_SESSION['loginedUser']['isAdmin'] == 1) {
                                        echo "Admin";
                                    } else {
                                        echo "User";
                                    }
                                    ?>
                                </span>
                            </a>

                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="singleMemberView.php?uniqueId=<?php echo $_SESSION['loginedUser']['uniqueId'] ?>"><i class="icon-user"></i> My profile</a></li>
                                <li><a href="logout.php"><i class="icon-switch2"></i> Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /main navbar -->


            <!-- Page container -->
            <div class="page-container">

                <!-- Page content -->
                <div class="page-content">

                    <!-- Main sidebar -->
                    <div class="sidebar sidebar-main">
                        <div class="sidebar-content">

                            <!-- User menu -->
                            <div class="sidebar-user">
                                <div class="category-content">
                                    <div class="media">
                                        <a href="#" class="media-left"><img src="images/massMemberImage/<?php echo $_SESSION['loginedUser']['image'] ?>" class="img-circle img-sm" alt=""></a>
                                        <div class="media-body">
                                            <span class="media-heading text-semibold"><?php echo ucfirst($_SESSION['loginedUser']['name']); ?></span>
                                            <div class="text-size-mini text-muted">
                                                <i class="icon-pin text-size-small"></i> &nbsp;L-12, Kazi Najrul Islam Road, Mohammadpur-1207
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /user menu -->


                            <!-- Main navigation -->
                            <div class="sidebar-category sidebar-category-visible">
                                <div class="category-content no-padding">
                                    <ul class="navigation navigation-main navigation-accordion">
                                        <!-- Main -->
                                        <li><a href="allMassMembers.php"><i class="icon-users4"></i> <span>Mass Members</span></a></li>
                                        <?php if ($_SESSION['loginedUser']['isAdmin'] == 1) { ?>
                                            <li><a href="memberAdd.php"><i class="icon-user-plus"></i> <span>Add Mass Members</span></a></li>
                                        <?php } ?>
                                        <li class="active"><a href="mealEntry.php"><i class="icon-droplets"></i> <span>Add Meal</span></a></li>
                                        <li><a href="shoppingEntry.php"><i class="icon-basket"></i> <span>Add Shopping</span></a></li>
                                        <li class="">
                                            <a href="#" class="has-ul"><i class="icon-tree7"></i> <span>Whole Mass Activities</span></a>
                                            <ul class="hidden-ul" style="display: none;">
                                                <li><a href="mealList.php"><i class="icon-stack2"></i> Meal Activities</a></li>
                                                <li><a href="shopping.php"><i class="icon-cart2"></i> Shopping Activities</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="finalReport.php"><i class="icon-briefcase3"></i> <span>Monthly Final Report</span></a></li>
                                        <li><a href="trashList.php"><i class="icon-blocked"></i> <span>Blocked Mass Members</span></a></li>
                                        <!-- /main -->
                                    </ul>
                                </div>
                            </div>
                            <!-- /main navigation -->
                        </div>
                        <!-- /sidebar-content -->
                    </div>
                    <!-- /main sidebar -->


                    <!-- Main content -->
                    <div class="content-wrapper">

                        <!-- Page header -->
                        <div class="page-header" style="margin-bottom: 0px;">
                            <div class="page-header-content">
                                <div class="page-title">
                                    <h4 class="custom-icon-size"><i class="icon-droplets position-left"></i> <span class="text-semibold">Add Meal (Meal Entry)</span></h4>
                                </div>
                            </div>

                            <div class="breadcrumb-line">
                                <ul class="breadcrumb">
                                    <li><a href="allMassMembers.php"><i class="icon-users4 position-left"></i>All Mass Members</a></li>
                                    <li class="active">Add Meal</li>
                                </ul>
                            </div>
                        </div>
                        <!-- /page header -->

                        <style type="text/css">
                            .breadcrumb > li + li:before {
                                content: "\f101 ";
                                font-family: FontAwesome;
                            }                            
                        </style>

                        <!-- Content area -->
                        <div class="content">

                            <?php if (!empty($_SESSION['trashSuccess'])) { ?>
                                <div class="alert alert-success alert-styled-left" style="margin: 0 10px 10px 10px; width: 50%; padding: 10px 14px;">
                                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                                    <?php $objResistration->msgEcho('trashSuccess'); ?>
                                </div>
                            <?php } ?>


                            <!-- Main charts -->
                            <div class="row">
                                <div id="mass-member-mesonary">
                                    <?php
                                    if (isset($allMemberData) && !empty($allMemberData)) {
                                        foreach ($allMemberData as $singleMemberData) {
                                            
                                        }
                                        ?>
                                        <form action="mealEntryProcess.php" method="POST">
                                            <div class="col-lg-12 single-mealMember">
                                                <div class="thumbnail">
                                                    <div class="caption">
                                                        <div class="member-image pull-left">
                                                            <div class="img-box"><img src="images/massMemberImage/<?php echo $singleMemberData['image'] ?>" height="100%" width="100%" alt="member image" /></div>
                                                            <h4 class="member-name"><?php echo $singleMemberData['name'] ?></h4>
                                                            <div class="text-left" style="margin-top: 14px; width: 100%; text-align: center;">
                                                                <button type="submit" class="btn btn-primary submit">Add Meal <i class="icon-droplet position-right"></i></button>                                                                                                                
                                                            </div>
                                                        </div>

                                                        <div class="meal-input-area pull-left">
                                                            <table>
                                                                <tbody>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <select name="memberId" class="form-control optionStyle">
                                                                                <option value="">--Select Mass Member--</option>
                                                                                <?php
                                                                                if (isset($allMemberData) && !empty($allMemberData)) {
                                                                                    foreach ($allMemberData as $singleMemberData) {
                                                                                        ?>
                                                                                        <option value="<?php echo $singleMemberData['id'] ?>" > <?php echo ucfirst($singleMemberData['name']) ?></option>
                                                                                    <?php }
                                                                                } else { ?>
                                                                                    <option>No Data Available</option>
                                                                                <?php } ?>
                                                                            </select>
                                                                            <p class="text-danger"><?php $objResistration->msgEcho('memberIdErr'); ?></p>
                                                                        </td>
                                                                    </tr>                                                                    
                                                                    <tr>
                                                                        <td><label> Breakfast <input type="checkbox" name="breakfastMeal" value="1"> </label></td>
                                                                        <td><input type="number" name="breakfastGuest" value="<?php
                                                                        if (isset($_SESSION['formData']['breakfastGuest'])) {
                                                                            echo $_SESSION['formData']['breakfastGuest'];
                                                                            unset($_SESSION['formData']['breakfastGuest']);
                                                                        }
                                                                                ?>" class="form-control" placeholder="Guest Meal"> <p class="text-danger"><?php $objResistration->msgEcho('brekfastErr'); ?></p> </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td><label> Lunch <input type="checkbox" name="lunchMeal" value="1"> </label></td>
                                                                        <td><input type="number" name="lunchGuest" value="<?php
                                                                           if (isset($_SESSION['formData']['lunchGuest'])) {
                                                                               echo $_SESSION['formData']['lunchGuest'];
                                                                               unset($_SESSION['formData']['lunchGuest']);
                                                                           }
                                                                                ?>" class="form-control" placeholder="Guest Meal"> <p class="text-danger"><?php $objResistration->msgEcho('lunchErr'); ?></p> </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td><label> Dinner <input type="checkbox" name="dinnerMeal" value="1"> </label></td>
                                                                        <td><input type="number" name="dinnerGuest" value="<?php
                                                                           if (isset($_SESSION['formData']['dinnerGuest'])) {
                                                                               echo $_SESSION['formData']['dinnerGuest'];
                                                                               unset($_SESSION['formData']['dinnerGuest']);
                                                                           }
                                                                                ?>" class="form-control" placeholder="Guest Meal"> <p class="text-danger"><?php $objResistration->msgEcho('dinnerErr'); ?></p> </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <div class="form-group">
                                                                                <div class='input-group date datetimepicker1' id='datetimepicker2'>
                                                                                    <input type='text' name="datetime" value="<?php
                                                                           if (isset($_SESSION['formData']['datetime'])) {
                                                                               echo $_SESSION['formData']['datetime'];
                                                                               unset($_SESSION['formData']['datetime']);
                                                                           }
                                                                                ?>" class="form-control" readonly="" placeholder="Date" required="" />
                                                                                    <span class="input-group-addon">
                                                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                                                    </span>
                                                                                </div>
                                                                                <p class="text-danger"> <?php $objResistration->msgEcho('dateErr');
                                                                                   $objResistration->msgEcho('repeatdateErr'); ?></p>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                                <!--/tbody-->
                                                            </table>
                                                            <!--/table-->
                                                        </div>

                                                        <div class="massageimg pull-left">
                                                            <div class="imgtbl">
                                                                <div class="imgtbl-cell">
                                                                    <?php if (isset($_SESSION['mealAddSuccessMSg'])) { ?>
                                                                        <img src="images/smile.gif" alt="" /> 
                                                                    <?php } ?>
                                                                    <p class="text-success"> <?php $objResistration->msgEcho('mealAddSuccessMSg'); ?></p>

                                                                    <?php if (isset($_SESSION['requiredimg'])) { ?>
                                                                        <img src="images/sadface.gif" alt="" /> 
                                                                    <?php } ?>
                                                                    <p class="text-danger"> <?php $objResistration->msgEcho('requiredimg'); ?></p>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="clear: both;"></div>
                                                    </div>
                                                    <!--/caption class-->
                                                </div>
                                                <!-- /thumbnail -->
                                            </div>
                                            <!-- /col-lg-12 -->

                                            <!--accordion content-->
                                        </form>

                                        <?php
                                    } else {
                                        echo"<h1 style='text-align:center'>No Meal Member Yet</h1>";
                                    }
                                    ?>

                                </div>
                                <!--/mesonary id-->
                            </div>
                            <!-- /main charts -->




                            <!-- Footer -->
                            <div class="footer text-muted">
                                &copy; 2016. <a href="#">Mass Managment Web App </a> by <a href="http://abbelal.tk" target="_blank">AB Belal></a>
                            </div>
                            <!-- /footer -->

                        </div>
                        <!-- /content area -->

                    </div>
                    <!-- /main content -->

                </div>
                <!-- /page content -->

            </div>
            <!-- /page container -->

            <!-- Latest compiled and minified JavaScript CDN -->
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

            <!-- Core JS files -->
            <script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
            <script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
            <script type="text/javascript" src="assets/js/core/app.js"></script>

            <!-- datetime picker js -->
            <script type="text/javascript" src="js/moment.js"></script>
            <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>


            <script type="text/javascript">
                /* datetime picker activation */
                $(function () {
                    $('.datetimepicker1').datetimepicker({
                        useCurrent:false,
                        format: "YYYY-MM-DD",
                        ignoreReadonly:true,
                         //minDate: new Date(),
                         maxDate: new Date()
                                                                                                                                                            			
                        /* if i want to select client only specify date then the code will
                minDate: "2016-02-20",
                maxDate: "2016-02-25" */
                    });
                });
                                                                                                                                                                                                                                                                                                              
                                                                                                                                                                
            </script>



        </body>
    </html>
    <?php
} else {
    $_SESSION['pageErr'] = "You have to login first";
    header('location:login.php');
}
?>