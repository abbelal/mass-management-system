<?php

include_once '../src/resistrationLogin/resistrationLogin.php';
$objResistration = new resistrationLogin();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $objResistration->prepare($_POST);
        $objResistration->formValidation();
        $objResistration->imageValidation();
        $objResistration->addMember();

    } else {
        $_SESSION['pageError'] = "<h1>404 page not found</h1>";
        header('location:error.php');
    }
?>
