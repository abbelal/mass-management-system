<?php

include_once '../src/massActivitis/massActivitis.php';
$objMassActivitis = new massActivitis();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $objMassActivitis->prepare($_POST);
    $objMassActivitis->shoppingValidation();
    $objMassActivitis->updateShopping();

} else {
    $_SESSION['pageError'] = "<h1>404 page not found</h1>";
    header('location:error.php');
}
?>
