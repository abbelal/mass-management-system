<?php
include_once '../src/resistrationLogin/resistrationLogin.php';
$objResistration = new resistrationLogin();

include_once '../src/massActivitis/massActivitis.php';
$objMassActivitis = new massActivitis();

$objResistration->prepare($_GET);
$singleMember = $objResistration->showSingleMember();

$objMassActivitis->prepare($_GET);
$singleMealMember = $objMassActivitis->showSingleMemberMeal();
$singleShoppingMember = $objMassActivitis->showSingleMemberShopping();


//echo '<pre>';
//print_r($singleShoppingMember);
//print_r($_SESSION['loginedUser']);
//******************************************
//shopping cost information
//******************************************
$totalShoppingCost = array_sum(array_column($singleShoppingMember, 'totalTk'));


//******************************************
//meal information
//******************************************
$breakFast = $lunch = $dinner = $totalGuest = 0;
foreach ($singleMealMember as $singleSingleMealMember) {

    $breakFast += $singleSingleMealMember['breakfastMeal'];
    $lunch += $singleSingleMealMember['lunchMeal'];
    $dinner += $singleSingleMealMember['dinnerMeal'];
    $totalGuest += $singleSingleMealMember['totalGuestMeal'];
}

$inTotalMeal = $breakFast + $lunch + $dinner + $totalGuest;
//$mealRate = 9.14;
$eatingCost = $inTotalMeal * $_SESSION['mealrate'];

if ($totalShoppingCost < $eatingCost) {
    $debitorCraditor = $eatingCost - $totalShoppingCost;
    $debitCradit = " Taka Debitor";
} elseif ($totalShoppingCost > $eatingCost) {
    $debitorCraditor = $totalShoppingCost - $eatingCost;
    $debitCradit = " Taka Creditor";
} else {
    $debitorCraditor = "Your Cost is label";
}
//******************************************
// end meal information
//******************************************




if (isset($_SESSION['loginedUser']) && !empty($_SESSION['loginedUser'])) {
    ?>

    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>Activities | <?php echo ucfirst($singleMember['name']); ?></title>
            <link rel="shortcut icon" type="image/png" href="images/favicon.png"/>

            <!-- google font CDN -->
            <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">

            <!--bootstrap CDN-->
            <!-- Latest compiled and minified CSS -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

            <!-- Optional theme -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

            <!-- Global stylesheets -->
            <link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
            <link href="assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
            <link href="assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
            <link href="assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
            <!-- /global stylesheets -->


            <!-- my all custom css file-->
            <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
            <link href="css/memberAddFromStyle.css" rel="stylesheet" type="text/css">
            <link href="css/singleviewpageStyle.css" rel="stylesheet" type="text/css">
            <link href="css/mealEntryFormDesign.css" rel="stylesheet" type="text/css">
            <link href="css/addShoppongStyle.css" rel="stylesheet" type="text/css">
            <link href="css/singleMassActivitiesStyle.css" rel="stylesheet" type="text/css">

        </head>

        <body>
            <!-- Main navbar -->
            <div class="navbar navbar-inverse custom-style">
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html"><img src="assets/images/app-loog.png" alt="app-logo"></a>

                    <ul class="nav navbar-nav visible-xs-block">
                        <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                        <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
                    </ul>

                </div>

                <div class="navbar-collapse collapse" id="navbar-mobile">
                    <ul class="nav navbar-nav">
                        <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown dropdown-user">
                            <a class="dropdown-toggle" data-toggle="dropdown">
                                <img height="46px" width="46px" src="images/massMemberImage/<?php echo $_SESSION['loginedUser']['image'] ?>" alt="user image">
                                <span><?php echo ucfirst($_SESSION['loginedUser']['name']); ?><br/></span>
                                <i class="caret"></i><br/>
                                <span class="admin">
                                    <?php
                                    if ($_SESSION['loginedUser']['isAdmin'] == 1) {
                                        echo "Admin";
                                    } else {
                                        echo "User";
                                    }
                                    ?>
                                </span>
                            </a>

                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="singleMemberView.php?uniqueId=<?php echo $_SESSION['loginedUser']['uniqueId'] ?>"><i class="icon-user"></i> My profile</a></li>
                                <li><a href="logout.php"><i class="icon-switch2"></i> Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /main navbar -->


            <!-- Page container -->
            <div class="page-container">

                <!-- Page content -->
                <div class="page-content">

                    <!-- Main sidebar -->
                    <div class="sidebar sidebar-main">
                        <div class="sidebar-content">

                            <!-- User menu -->
                            <div class="sidebar-user">
                                <div class="category-content">
                                    <div class="media">
                                        <a href="#" class="media-left"><img src="images/massMemberImage/<?php echo $_SESSION['loginedUser']['image'] ?>" class="img-circle img-sm" alt=""></a>
                                        <div class="media-body">
                                            <span class="media-heading text-semibold"><?php echo ucfirst($_SESSION['loginedUser']['name']); ?></span>
                                            <div class="text-size-mini text-muted">
                                                <i class="icon-pin text-size-small"></i> &nbsp;L-12, Kazi Najrul Islam Road, Mohammadpur-1207
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /user menu -->


                            <!-- Main navigation -->
                            <div class="sidebar-category sidebar-category-visible">
                                <div class="category-content no-padding">
                                    <ul class="navigation navigation-main navigation-accordion">
                                        <!-- Main -->
                                        <li class="active"><a href="allMassMembers.php"><i class="icon-users4"></i> <span>Mass Members</span></a></li>
                                        <?php if ($_SESSION['loginedUser']['isAdmin'] == 1) { ?>
                                            <li><a href="memberAdd.php"><i class="icon-user-plus"></i> <span>Add Mass Members</span></a></li>
                                        <?php } ?>
                                        <li><a href="mealEntry.php"><i class="icon-droplets"></i> <span>Add Meal</span></a></li>
                                        <li><a href="shoppingEntry.php"><i class="icon-basket"></i> <span>Add Shopping</span></a></li>
                                        <li class="">
                                            <a href="#" class="has-ul"><i class="icon-tree7"></i> <span>Whole Mass Activities</span></a>
                                            <ul class="hidden-ul" style="display: none;">
                                                <li><a href="mealList.php"><i class="icon-stack2"></i> Meal Activities</a></li>
                                                <li><a href="shopping.php"><i class="icon-cart2"></i> Shopping Activities</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="finalReport.php"><i class="icon-briefcase3"></i> <span>Monthly Final Report</span></a></li>
                                        <li><a href="trashList.php"><i class="icon-blocked"></i> <span>Blocked Mass Members</span></a></li>
                                        <!-- /main -->
                                    </ul>
                                </div>
                            </div>
                            <!-- /main navigation -->
                        </div>
                        <!-- /sidebar-content -->
                    </div>
                    <!-- /main sidebar -->


                    <!-- Main content -->
                    <div class="content-wrapper">

                        <!-- Page header -->
                        <div class="page-header">
                            <div class="page-header-content">
                                <div class="page-title pull-left">
                                    <h4 class="custom-icon-size"><i class="icon-fire position-left"></i> <span class="text-semibold">Mass Activities for <?php echo ucfirst($singleMember['name']); ?></span></h4>
                                </div>
                                <div class="dataFilterSearch pull-right">
                                    <form class="form-inline">
                                        <div class="form-group">
                                            <select class="form-control">
                                                <option value="" >-- Select Month --</option>
                                                <option value="" >January</option>
                                                <option value="">February</option>
                                                <option value="">March</option>
                                                <option value="">April</option>
                                                <option value="">May</option>
                                                <option value="">Jun</option>
                                                <option value="">July</option>
                                                <option value="">August</option>
                                                <option value="">September</option>
                                                <option value="">October</option>
                                                <option value="">November</option>
                                                <option value="">December</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control">
                                                <option value="">-- Select Year --</option>
                                                <option value="">2017</option>
                                                <option value="">2016</option>
                                                <option value="">2015</option>
                                                <option value="">2014</option>
                                                <option value="">2013</option>
                                                <option value="">2012</option>
                                                <option value="">2011</option>
                                                <option value="">2010</option>
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-default">Search</button>
                                    </form>
                                    <!-- /search form -->
                                </div>
                                <div style="clear: both;"></div>
                            </div>

                            <div class="breadcrumb-line">
                                <ul class="breadcrumb">
                                    <li><a href="allMassMembers.php"><i class="icon-users4 position-left"></i>All Mass Members</a></li>
                                    <li class="active">Single Mass Activities</li>
                                </ul>
                            </div>
                        </div>
                        <!-- /page header -->


                        <!-- Content area -->
                        <div class="content">

                            <!-- Main charts -->
                            <div class="row">
                                <div id="singleMemberinfo">
                                    <?php if (isset($singleMember) && !empty($singleMember)) { ?>
                                        <div class="col-lg-4">
                                            <div class="thumbnail">
                                                <div class="thumb pull-left">
                                                    <img src="images/massMemberImage/<?php echo $singleMember['image'] ?>" alt="member image">
                                                </div>
                                                <div class="thum-name pull-left">
                                                    <h2><?php echo $singleMember['name'] ?></h2>
                                                    <p>
                                                        <span>
                                                            <?php
                                                            echo round($debitorCraditor, 2);
                                                            if (isset($debitCradit)) {
                                                                echo $debitCradit;
                                                            }
                                                            ?>
                                                        </span> 
                                                    </p>
                                                </div>
                                                <div style="clear: both;"></div>
                                            </div>
                                            <!-- /traffic sources -->

                                            <div class="thumbnail shopping-cost">
                                                <div class="thumb">
                                                    <h2>shopping cost for</h2>
                                                    <p>Octobar-2016</p>
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered">
                                                            <thead> 
                                                                <tr> 
                                                                    <th>Date</th> 
                                                                    <th>Shopping Details</th> 
                                                                    <th>Taka</th>   
                                                                    <th>Action</th>   
                                                                </tr> 
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                if (isset($singleShoppingMember) && !empty($singleShoppingMember)) {
                                                                    foreach ($singleShoppingMember as $singleSingleShoppingMember) {
                                                                        //print_r($singleSingleShoppingMember);
                                                                        ?>
                                                                        <tr>
                                                                            <td><?php echo $singleSingleShoppingMember['shoppingDate'] ?></td>
                                                                            <td><?php echo $singleSingleShoppingMember['shoppingDiscription'] ?></td>
                                                                            <td><?php echo $singleSingleShoppingMember['totalTk']; ?></td>
                                                                            <?php if ($_SESSION['loginedUser']['isAdmin'] == 1 || $singleSingleShoppingMember ['uniqueId'] == $_SESSION['loginedUser']['uniqueId'] && $singleSingleShoppingMember ['isDelete'] == 0) { ?>
                                                                                <td><button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#id<?php echo $singleSingleShoppingMember['id']; ?>">Edit</button></td>
                                                                            <?php } else { ?>
                                                                                <td><button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="" disabled="">Edit</button></td>
                                                                            <?php } ?>   
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                } else {
                                                                    echo "<h1 style='text-align:center;'>No Shopping Yet..!</h1>";
                                                                }
                                                                ?>
                                                            </tbody>
                                                            <tfoot>
                                                                <tr>
                                                                    <td colspan="2">Total Shopping Cost</td>
                                                                    <td colspan="2" style="text-align: left;"><?php echo $totalShoppingCost; ?> Taka</td>
                                                                </tr>
                                                            </tfoot>
                                                        </table>
                                                    </div>
                                                </div>                                                
                                            </div>
                                            <!-- /traffic sources -->

                                            <!--final report-->
                                            <div class="thumbnail shopping-cost">
                                                <div class="thumb">
                                                    <h2>Final Meal Report for</h2>
                                                    <p>Octobar-2016</p>
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered">
                                                            <tfoot>
                                                                <tr>
                                                                    <td>Total Shopping Cost</td>                                                                    
                                                                    <td style="text-align: left;"><?php echo $totalShoppingCost; ?> Taka</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>In Total Meal</td>                                                                    
                                                                    <td style="text-align: left;">
                                                                        <?php echo $inTotalMeal; ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Meal Rate</td>                                                                    
                                                                    <td style="text-align: left;">
                                                                        <?php
                                                                        if (isset($_SESSION['mealrate'])) {
                                                                            echo round($_SESSION['mealrate'], 2);
                                                                        }
                                                                        ?> Taka
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Your Total Eating Cost</td>                                                                    
                                                                    <td style="text-align: left; color: red;">
                                                                        <?php echo round($eatingCost, 2); ?> Taka
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Your are</td>                                                                    
                                                                    <td style="text-align: left; color: red;">
                                                                        <?php
                                                                        echo round($debitorCraditor, 2);
                                                                        if (isset($debitCradit)) {
                                                                            echo $debitCradit;
                                                                        }
                                                                        ?>
                                                                    </td>
                                                                </tr>
                                                            </tfoot>
                                                        </table>
                                                    </div>
                                                </div>                                                
                                            </div>
                                            <!-- /traffic sources -->

                                        </div> 

                                        <div class="col-lg-8">
                                            <div class="thumbnail">
                                                <div class="thumb">
                                                    <div class="caption yourmeal">
                                                        <h2>all meal for <span>October-2016</span></h2> 
                                                        <div class="table-responsive">
                                                            <table class="table table-bordered">
                                                                <thead> 
                                                                    <tr> 
                                                                        <th>Date</th> 
                                                                        <th>Breakfast Meal</th> 
                                                                        <th>Lunch Meal</th> 
                                                                        <th>Dinner Meal</th> 
                                                                        <th>Total Guest Meal</th>   
                                                                        <th>Action</th>   
                                                                    </tr> 
                                                                </thead>
                                                                <tbody>
                                                                    <?php
                                                                    if (isset($singleMealMember) && !empty($singleMealMember)) {
                                                                        foreach ($singleMealMember as $singleSingleMealMember) {
                                                                            ?>
                                                                            <tr>
                                                                                <td><?php echo $singleSingleMealMember['mealDate'] ?></td>
                                                                                <td><?php echo $singleSingleMealMember['breakfastMeal'] ?></td>
                                                                                <td><?php echo $singleSingleMealMember['lunchMeal'] ?></td>
                                                                                <td><?php echo $singleSingleMealMember['dinnerMeal'] ?></td>
                                                                                <td><?php echo $singleSingleMealMember['totalGuestMeal'] ?></td>
                                                                                <?php if ($_SESSION['loginedUser']['isAdmin'] == 1 || $singleSingleShoppingMember ['uniqueId'] == $_SESSION['loginedUser']['uniqueId'] && $singleSingleShoppingMember ['isDelete'] == 0) { ?>
                                                                                    <td><button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#id<?php echo $singleSingleMealMember['id']; ?>">Edit</button></td>
                                                                                <?php } else { ?>
                                                                                    <td><button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="" disabled="">Edit</button></td>
                                                                                <?php } ?> 
                                                                            </tr>
                                                                            <?php
                                                                        }
                                                                    } else {
                                                                        echo "<h1 style='text-align:center;'>No Meal Yet..!</h1>";
                                                                    }
                                                                    ?>
                                                                </tbody>
                                                                <tfoot>
                                                                    <tr>
                                                                        <td>Total</td>
                                                                        <td><?php echo $breakFast; ?></td>
                                                                        <td><?php echo $lunch; ?></td>
                                                                        <td><?php echo $dinner; ?></td>
                                                                        <td class="text-center"><?php echo $totalGuest; ?></td>

                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="5">
                                                                            <p class="text-center" style="margin:0px;">
                                                                                In Total Meal : <?php echo $inTotalMeal; ?>
                                                                            </p>
                                                                        </td>
                                                                    </tr>
                                                                </tfoot>

                                                            </table>
                                                        </div>
                                                        <!--/tbl responsive-->
                                                    </div>
                                                </div>
                                                <div class="text-center actionbtn">
                                                    <a href="allMassMembers.php" style="margin-right: 5px;" type="button" class="btn btn-default"><i class="icon-undo2  position-left"></i>Back to All Members</a>
                                                    <a href="trashList.php" style="margin-right: 5px;" type="button" class="btn btn-default"><i class="icon-undo2  position-left"></i>Back to Blocked Members</a>
                                                </div>
                                            </div>
                                            <!-- /traffic sources -->
                                        </div> 
                                        <?php
                                    } else {
                                        echo"<h2>No Mass Member Meal Yet </h2>";
                                    }
                                    ?>
                                </div>
                                <!--/mesonary id-->
                            </div>
                            <!-- /main charts -->



                            <!-- Footer -->
                            <div class="footer text-muted">
                                &copy; 2016. <a href="#">Mass Managment Web App </a> by <a href="http://abbelal.tk" target="_blank">AB Belal</a>
                            </div>
                            <!-- /footer -->

                        </div>
                        <!-- /content area -->

                    </div>
                    <!-- /main content -->

                </div>
                <!-- /page content -->

            </div>
            <!-- /page container -->

            <?php foreach ($singleMealMember as $memberList) { ?>

                <!-- Modal for meal edit-->
                <div id="id<?php echo $memberList['id']; ?>" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content mealEdit">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Modal Header</h4>
                                <?php //print_r($memberList) ;            ?>
                            </div>
                            <div class="modal-body">
                                <form action="mealUpdateProcess.php" method="POST">
                                    <div class="col-lg-12 single-mealMember">
                                        <div class="thumbnail">
                                            <div class="caption">
                                                <div class="member-image pull-left">
                                                    <div class="img-box"><img src="images/massMemberImage/<?php echo $memberList['image'] ?>" height="100%" width="100%" alt="member image" /></div>
                                                    <h4 class="member-name"><?php echo $memberList['name'] ?></h4>
                                                    <div class="text-left" style="margin-top: 14px; width: 100%; text-align: center;">
                                                        <button type="submit" class="btn btn-primary submit">Update Meal <i class="icon-check2 position-right"></i></button>                                                                                                                
                                                    </div>
                                                    <p class="text-danger"> <?php $objResistration->msgEcho('mealUpdateSuccessMSg'); ?></p>
                                                </div>
                                                <div class="meal-input-area pull-right">
                                                    <table>
                                                        <tbody>
                                                        <input type="hidden" name="id" value="<?php echo $memberList['id'] ?>" class="form-control" placeholder="member id" >
                                                        <input type="hidden" name="memberId" value="<?php echo $memberList['userId'] ?>" class="form-control" placeholder="member id" >
                                                        <input type="hidden" name="uniqueId" value="<?php echo $memberList['uniqueId'] ?>" class="form-control" placeholder="member uniqueId" >
                                                        <tr>
                                                            <td><label> Breakfast <input type="checkbox" name="breakfastMeal" value="1" <?php
                        if ($memberList['breakfastMeal'] == 1) {
                            echo 'checked';
                        }
                                ?> > </label></td>
                                                            <td><input type="number" min="0" name="breakfastGuest" value="<?php
                                                                                 if (isset($memberList['breakfastGuest'])) {
                                                                                     echo $memberList['breakfastGuest'];
                                                                                 }
                                ?>" class="form-control"> <p class="text-danger"><?php $objResistration->msgEcho('brekfastErr'); ?></p> </td>
                                                        </tr>

                                                        <tr>
                                                            <td><label> Lunch <input type="checkbox" name="lunchMeal" value="1" <?php
                                                               if ($memberList['lunchMeal'] == 1) {
                                                                   echo 'checked';
                                                               }
                                ?> > </label></td>
                                                            <td><input type="number" min="0" name="lunchGuest" value="<?php
                                                                             if (isset($memberList['lunchGuest'])) {
                                                                                 echo $memberList['lunchGuest'];
                                                                             }
                                ?>" class="form-control"> <p class="text-danger"><?php $objResistration->msgEcho('lunchErr'); ?></p> </td>
                                                        </tr>

                                                        <tr>
                                                            <td><label> Dinner <input type="checkbox" name="dinnerMeal" value="1" <?php
                                                               if ($memberList['dinnerMeal'] == 1) {
                                                                   echo 'checked';
                                                               }
                                ?> > </label></td>
                                                            <td><input type="number" min="0" name="dinnerGuest" value="<?php
                                                                              if (isset($memberList['dinnerGuest'])) {
                                                                                  echo $memberList['dinnerGuest'];
                                                                              }
                                ?>" class="form-control"> <p class="text-danger"><?php $objResistration->msgEcho('dinnerErr'); ?></p> </td>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="2">
                                                                <div class="form-group">
                                                                    <div class='input-group date datetimepicker1' id='datetimepicker2'>
                                                                        <input type='text' name="datetime" value="<?php
                                                               if (isset($memberList ['mealDate'])) {
                                                                   echo $memberList ['mealDate'];
                                                               }
                                ?>" class="form-control" readonly="" placeholder="Date" required="" />
                                                                        <span class="input-group-addon">
                                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                                        </span>
                                                                    </div>
                                                                    <p class="text-danger"> <?php $objResistration->msgEcho('dateErr');
                                                                       $objResistration->msgEcho('repeatdateErr'); ?></p>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                        <!--/tbody-->
                                                    </table>
                                                    <!--/table-->
                                                </div>
                                                <div style="clear: both;"></div>
                                            </div>
                                            <!--/caption class-->
                                        </div>
                                        <!-- /thumbnail -->
                                    </div>
                                    <!-- /col-lg-12 -->

                                    <!--accordion content-->
                                </form>
                            </div>
                            <div class="modal-footer">
                                <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                            </div>
                        </div>

                    </div>
                </div>
            <?php } ?>
            <!--end meal edit modal-->


            <?php foreach ($singleShoppingMember as $shoppingMemberList) { ?>
                <?php //print_r($shoppingMemberList);            ?>

                <!-- Modal for Shopping edit-->
                <div id="id<?php echo $shoppingMemberList['id']; ?>" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content shoppingEdit">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Shopping Edit</h4>
                                <?php //print_r($shoppingMemberList);               ?>
                            </div>
                            <div class="modal-body">
                                <form action="shoppingEditProcess.php" method="POST">
                                    <div class="thumbnail">
                                        <div class="caption">
                                            <div class="member-image pull-left">
                                                <div class="img-box"><img src="images/massMemberImage/<?php echo $shoppingMemberList['image'] ?>" height="100%" width="100%" alt="member image" /></div>
                                                <h4 class="member-name"><?php echo $shoppingMemberList['name'] ?></h4>
                                                <p class="text-success"> <?php $objResistration->msgEcho('mealAddSuccessMSg'); ?></p>
                                                <div class="text-left" style="margin-top: 14px; width: 100%; text-align: center;">
                                                    <button type="submit" class="btn btn-primary submit">Update Shopping <i class="icon-check2 position-right"></i></button>                                                                                                                
                                                </div>
                                            </div>

                                            <div class="meal-input-area pull-right">
                                                <table>
                                                    <tbody>
                                                    <input type="hidden" name="id" value="<?php echo $shoppingMemberList['id'] ?>" class="form-control" placeholder="member id">
                                                    <input type="hidden" name="memberId" value="<?php echo $shoppingMemberList['userId'] ?>" class="form-control" placeholder="member id">
                                                    <input type="hidden" name="uniqueId" value="<?php echo $shoppingMemberList['uniqueId'] ?>" class="form-control" placeholder="member id">
                                                    <tr>
                                                        <td>
                                                            <textarea name="shoppingDisription" class="form-control" rows="2" placeholder="Type Your Shopping Discription..."><?php
                        if (isset($shoppingMemberList['shoppingDiscription'])) {
                            echo $shoppingMemberList['shoppingDiscription'];
                        }
                                ?> </textarea>
                                                            <p class="text-danger"><?php $objResistration->msgEcho(''); ?></p> 
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>                                                            
                                                            <input type="number" min="0" name="shoppingTk" value="<?php
                                                        if (isset($shoppingMemberList['totalTk'])) {
                                                            echo $shoppingMemberList['totalTk'];
                                                        }
                                ?>" class="form-control">
                                                            <p class="text-danger"><?php $objResistration->msgEcho('shopTKErrr'); ?></p>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="2">                                                            
                                                            <div class="form-group">
                                                                <div class='input-group date datetimepicker1' id='datetimepicker2'>
                                                                    <input style="cursor: not-allowed;" type='text' name="datetime" value="<?php
                                                           if (isset($shoppingMemberList ['shoppingDate'])) {
                                                               echo $shoppingMemberList ['shoppingDate'];
                                                           }
                                ?>" class="form-control" readonly="" placeholder="Date" required="" />
                                                                    <span class="input-group-addon" style="cursor: pointer;">
                                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                                    </span>
                                                                </div>
                                                                <p class="text-danger"> <?php $objResistration->msgEcho('dateErrr');
                                                                   $objResistration->msgEcho('repeatdateErrr'); ?></p>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                    <!--/tbody-->
                                                </table>
                                                <!--/table-->
                                            </div>
                                            <div style="clear: both;"></div>
                                        </div>
                                        <!--/caption class-->
                                    </div>
                                    <!-- /thumbnail -->
                                </form>
                            </div>
                            <div class="modal-footer">
                                <!--                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                            </div>
                        </div>

                    </div>
                </div>
            <?php } ?>
            <!--end Shopping edit modal-->


            <!-- Latest compiled and minified JavaScript CDN -->
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

            <!-- Core JS files -->
            <script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
            <script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
            <script type="text/javascript" src="assets/js/core/app.js"></script>

            <!-- datetime picker js -->
            <script type="text/javascript" src="js/moment.js"></script>
            <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>


            <script type="text/javascript">
                /* datetime picker activation */
                $(function () {
                    $('.datetimepicker1').datetimepicker({
                        useCurrent:false,
                        format:"DD-MMM-YYYY",
                        ignoreReadonly:true,
                        maxDate: new Date()
                                                                                                                                                                                                                                                                                                                                            			
                        /* if i want to select client only specify date then the code will beauty-carousel
                minDate: "2016-02-20",
                maxDate: "2016-02-25" */
                    });
                });
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
                                                                                                                                                                                                                                                                                                                                                
            </script>

        </body>
    </html>
    <?php
} else {
    $_SESSION['pageErr'] = "You have to login first";
    header('location:login.php');
}
?>