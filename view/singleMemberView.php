<?php
include_once '../src/resistrationLogin/resistrationLogin.php';
$objResistration = new resistrationLogin();

$objResistration->prepare($_GET);
$singleMember = $objResistration->showSingleMember();
//print_r($singleMember);

//print_r($_SESSION['loginedUser']);
if (isset($_SESSION['loginedUser']) && !empty($_SESSION['loginedUser'])) {
    ?>

    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title><?php echo ucfirst($singleMember['name']); ?></title>
            <link rel="shortcut icon" type="image/png" href="images/favicon.png"/>

            <!-- google font CDN -->
            <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">

            <!--bootstrap CDN-->
            <!-- Latest compiled and minified CSS -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

            <!-- Optional theme -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

            <!-- Global stylesheets -->
            <link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
            <link href="assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
            <link href="assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
            <link href="assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
            <!-- /global stylesheets -->


            <!-- my all custom css file-->
            <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
            <link href="css/memberAddFromStyle.css" rel="stylesheet" type="text/css">
            <link href="css/singleviewpageStyle.css" rel="stylesheet" type="text/css">

        </head>

        <body>
            <!-- Main navbar -->
            <div class="navbar navbar-inverse custom-style">
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html"><img src="assets/images/app-loog.png" alt="app-logo"></a>

                    <ul class="nav navbar-nav visible-xs-block">
                        <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                        <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
                    </ul>

                </div>

                <div class="navbar-collapse collapse" id="navbar-mobile">
                    <ul class="nav navbar-nav">
                        <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown dropdown-user">
                            <a class="dropdown-toggle" data-toggle="dropdown">
                                <img height="46px" width="46px" src="images/massMemberImage/<?php echo $_SESSION['loginedUser']['image'] ?>" alt="user image">
                                <span><?php echo ucfirst($_SESSION['loginedUser']['name']); ?><br/></span>
                                <i class="caret"></i><br/>
                                <span class="admin">
                                    <?php
                                    if ($_SESSION['loginedUser']['isAdmin'] == 1) {
                                        echo "Admin";
                                    } else {
                                        echo "User";
                                    }
                                    ?>
                                </span>
                            </a>

                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="singleMemberView.php?uniqueId=<?php echo $_SESSION['loginedUser']['uniqueId'] ?>"><i class="icon-user"></i> My profile</a></li>
                                <li><a href="logout.php"><i class="icon-switch2"></i> Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /main navbar -->


            <!-- Page container -->
            <div class="page-container">

                <!-- Page content -->
                <div class="page-content">

                    <!-- Main sidebar -->
                    <div class="sidebar sidebar-main">
                        <div class="sidebar-content">

                            <!-- User menu -->
                            <div class="sidebar-user">
                                <div class="category-content">
                                    <div class="media">
                                        <a href="#" class="media-left"><img src="images/massMemberImage/<?php echo $_SESSION['loginedUser']['image'] ?>" class="img-circle img-sm" alt=""></a>
                                        <div class="media-body">
                                            <span class="media-heading text-semibold"><?php echo ucfirst($_SESSION['loginedUser']['name']); ?></span>
                                            <div class="text-size-mini text-muted">
                                                <i class="icon-pin text-size-small"></i> &nbsp;L-12, Kazi Najrul Islam Road, Mohammadpur-1207
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /user menu -->


                            <!-- Main navigation -->
                            <div class="sidebar-category sidebar-category-visible">
                                <div class="category-content no-padding">
                                    <ul class="navigation navigation-main navigation-accordion">
                                        <!-- Main -->
                                        <li class="active"><a href="allMassMembers.php"><i class="icon-users4"></i> <span>Mass Members</span></a></li>
                                        <?php if ($_SESSION['loginedUser']['isAdmin'] == 1) { ?>
                                            <li><a href="memberAdd.php"><i class="icon-user-plus"></i> <span>Add Mass Members</span></a></li>
                                        <?php } ?>
                                        <li><a href="mealEntry.php"><i class="icon-droplets"></i> <span>Add Meal</span></a></li>
                                        <li><a href="shoppingEntry.php"><i class="icon-basket"></i> <span>Add Shopping</span></a></li>
                                        <li class="">
                                            <a href="#" class="has-ul"><i class="icon-tree7"></i> <span>Whole Mass Activities</span></a>
                                            <ul class="hidden-ul" style="display: none;">
                                                <li><a href="mealList.php"><i class="icon-stack2"></i> Meal Activities</a></li>
                                                <li><a href="shopping.php"><i class="icon-cart2"></i> Shopping Activities</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="finalReport.php"><i class="icon-briefcase3"></i> <span>Monthly Final Report</span></a></li>
                                        <li><a href="trashList.php"><i class="icon-blocked"></i> <span>Blocked Mass Members</span></a></li>
                                        <!-- /main -->
                                    </ul>
                                </div>
                            </div>
                            <!-- /main navigation -->
                        </div>
                        <!-- /sidebar-content -->
                    </div>
                    <!-- /main sidebar -->


                    <!-- Main content -->
                    <div class="content-wrapper">

                        <!-- Page header -->
                        <div class="page-header">
                            <div class="page-header-content">
                                <div class="page-title">
                                    <h4 class="custom-icon-size"><i class="icon-grid5 position-left"></i> <span class="text-semibold">All Information of <?php echo ucfirst($singleMember['name']); ?></span></h4>
                                </div>
                            </div>

                            <div class="breadcrumb-line">
                                <ul class="breadcrumb">
                                    <li><a href="allMassMembers.php"><i class="icon-users4 position-left"></i>All Mass Members</a></li>
                                    <li class="active">Single Members Information</li>
                                </ul>
                            </div>
                        </div>
                        <!-- /page header -->


                        <!-- Content area -->
                        <div class="content">

                            <!-- Main charts -->
                            <div class="row">
                                <div id="singleMemberinfo">
                                    <?php if (isset($singleMember) && !empty($singleMember)) { ?>
                                        <div class="col-lg-4">
                                            <div class="thumbnail">
                                                <div class="thumb">
                                                    <img src="images/massMemberImage/<?php echo $singleMember['image'] ?>" alt="member image">
                                                </div>

                                                <!--action button-->
                                                <div class="text-center actionbtn">
                                                    <?php if ($_SESSION['loginedUser']['id'] == $singleMember['id'] || $_SESSION['loginedUser']['isAdmin'] == 1 && $singleMember['isDelete'] == 0) { ?>   
                                                        <a style="margin-right: 5px;" type="button" class="btn btn-default" href="edit.php?uniqueId=<?php echo $singleMember['uniqueId'] ?>"><i class="icon-pencil5  position-left"></i>Edit</a>
                                                    <?php } ?>

                                                    <a style="margin-right: 5px;" type="button" class="btn btn-default" href="singleMassActivities.php?uniqueId=<?php echo $singleMember['uniqueId'] ?>"><i class="icon-reading  position-left"></i>Mass Activities</a>

                                                    <?php if ($_SESSION['loginedUser']['isAdmin'] == 1 && $singleMember['isDelete'] == 0) { ?>
                                                        <a type="button" class="btn btn-default" href="trash.php?uniqueId=<?php echo $singleMember['uniqueId'] ?>" onclick="return confirm('Are you sure to Block this Member ?')"><i class="icon-blocked position-left"></i> Block</a>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <!-- /traffic sources -->

                                        </div> 

                                        <div class="col-lg-8">
                                            <div class="thumbnail">
                                                <div class="thumb">
                                                    <div class="caption">
                                                        <div class="text-left">

                                                            <!--member all information-->
                                                            <h4 style="margin-bottom: 5px!important;" class="text-semibold no-margin member-name"><?php echo ucfirst($singleMember['name']); ?></h4>
                                                            <dl class="dl-horizontal">
                                                                <dt>Father's Name</dt><dd>:&nbsp; <?php echo $singleMember['fatherName']; ?></dd>
                                                                <dt>Education</dt><dd>:&nbsp; <?php echo $singleMember['education']; ?></dd>
                                                                <dt>Email</dt><dd>:&nbsp; <?php echo $singleMember['email']; ?></dd>
                                                                <dt>Password</dt>
                                                                <dd>:&nbsp; 
                                                                    <?php
                                                                    if ($_SESSION['loginedUser']['id'] == $singleMember['id'] || $_SESSION['loginedUser']['isAdmin'] == 1) {
                                                                        echo $singleMember['password'];
                                                                    } else {
                                                                        echo "You are not Authorized to see Password";
                                                                    }
                                                                    ?>
                                                                </dd>
                                                                <dt>Phone</dt><dd>:&nbsp; <?php echo "0" . $singleMember['phone']; ?></dd>
                                                                <dt>National Id</dt><dd>:&nbsp; <?php echo $singleMember['nationalid']; ?></dd>
                                                                <dt>Address</dt>
                                                                <dd>:&nbsp; 
                                                                    <?php
                                                                    $addressSerializ = (unserialize($singleMember['address']));
                                                                    echo $addressSerializ[0] . ", " . $addressSerializ[1] . ", " . $addressSerializ[2];
                                                                    ?>
                                                                </dd>
                                                                <dt>Joining Date</dt><dd>:&nbsp; <?php echo $singleMember['date']; ?></dd>
                                                                <dt>Position</dt>
                                                                <dd>:&nbsp; 
                                                                    <?php
                                                                    if ($singleMember['isAdmin'] == 1) {
                                                                        echo "Admin";
                                                                    } else {
                                                                        echo "User";
                                                                    }
                                                                    ?>
                                                                </dd>
                                                                <dt>Member Status</dt>
                                                                <dd>:&nbsp; 
                                                                    <?php
                                                                    if ($singleMember['isDelete'] == 1) {
                                                                        echo "Old Member (Blocked)";
                                                                    } else {
                                                                        echo "New Member (He's Still There)";
                                                                    }
                                                                    ?>
                                                                </dd>
                                                            </dl>
                                                        </div>
                                                        <ul class="list-unstyled list-icons">
                                                            <li><span>Profile Created Date</span> <?php echo $singleMember['created']; ?></li>
                                                            <li><span class="upd">Profile Updated Date</span> <?php echo $singleMember['updated']; ?></li>
                                                        </ul>

                                                    </div>
                                                </div>
                                                <div class="text-center actionbtn">
                                                    <a href="allMassMembers.php" style="margin-right: 5px;" type="button" class="btn btn-default"><i class="icon-undo2  position-left"></i>Back to All Members</a>
                                                    <a href="trashList.php" style="margin-right: 5px;" type="button" class="btn btn-default"><i class="icon-undo2  position-left"></i>Back to Blocked Members</a>
                                                </div>
                                            </div>
                                            <!-- /traffic sources -->
                                        </div> 
                                        <?php
                                    } else {
                                        echo"<h2>No Mass Member Information Yet </h2>";
                                    }
                                    ?>
                                </div>
                                <!--/mesonary id-->
                            </div>
                            <!-- /main charts -->



                            <!-- Footer -->
                            <div class="footer text-muted">
                                &copy; 2016. <a href="#">Mass Managment Web App </a> by <a href="http://abbelal.tk" target="_blank">AB Belal</a>
                            </div>
                            <!-- /footer -->

                        </div>
                        <!-- /content area -->

                    </div>
                    <!-- /main content -->

                </div>
                <!-- /page content -->

            </div>
            <!-- /page container -->

            <!-- Latest compiled and minified JavaScript CDN -->
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

            <!-- Core JS files -->
            <script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
            <script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
            <script type="text/javascript" src="assets/js/core/app.js"></script>

        </body>
    </html>
    <?php
} else {
    $_SESSION['pageErr'] = "You have to login first";
    header('location:login.php');
}
?>