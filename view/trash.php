<?php

include_once '../src/resistrationLogin/resistrationLogin.php';
$objResistration = new resistrationLogin();

if (isset($_SESSION['loginedUser']) && !empty($_SESSION['loginedUser'])) {
    if ($_SERVER['REQUEST_METHOD'] == 'GET') {
        $objResistration->prepare($_GET);
        $objResistration->memberBlock();
        
    } else {
        $_SESSION['pageErr'] = "<h1>404 page not found <a href='login.php'>Click to Login</a></h1>";
        header('location:error.php');
    }
} else {
    $_SESSION['loginErr'] = "You have to Login first";
    header('location:login.php');
}
?>
